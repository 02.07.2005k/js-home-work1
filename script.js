"use strict"

// ЗАВДАННЯ 1
let variableName; // Оголошення змінної без присвоєння значення

//const constantName = 'value';  Оголошення змінної const з незмінним (константним) значенням.
// Оголошення змінної var.
// var variableName = 'value';  Застарілий метод оголошення змінних. Має глобальний контекст.

// 2. Рядок (string) - це послідовність символів. Можна створити рядок, використовуючи різні способи:
let str1 = 'Це рядок одинарних лапок.';
console.log(str1);
let str2 = "Це рядок подвійних лапок.";
console.log(str2);
let str3 = `Це рядок з обернених лапок (шаблонний рядок).`;
console.log(str3);

// 3. Перевірка типу даних змінної в JavaScript:
let variableToCheck = 42;
console.log(typeof variableToCheck); // Виведе 'number', оскільки variableToCheck містить число.

// 4. Пояснення чому '1' + 1 = 11:
// У JavaScript, коли ви додаєте рядок до числа, JavaScript автоматично конвертує число у рядок і проводить конкатенацію (об'єднання) рядків, а не виконує додавання чисел. Таким чином, '1' + 1 стає '11'.
let result = '1' + 1;
console.log(result); // Виведе '11'

// ЗАВДАННЯ 2
let numberVariable = 42;
console.log(typeof numberVariable === 'number'); 

let name = 'Yroslav';
let lastName = 'Kolisnuk';
console.log(`Мене звати ${name}, ${lastName}`); 

let numericValue = 10;
console.log(`Значення змінної: ${numericValue}`); 
